import { PhotosService } from './../../services/photos.service';
import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-albums',
  templateUrl: './albums.component.html',
  styleUrls: ['./albums.component.css']
})
export class AlbumsComponent implements OnInit {

  albums: Observable<Object> | undefined;

  constructor(private photoService: PhotosService) { }

  ngOnInit(): void {
    this.albums = this.photoService.getAlbums();    
  }

}
